package org.example.manager;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import org.example.data.FlatModel;
import org.example.dto.RequestModel;
import org.example.exception.FlatNotFoundException;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

public class RealtyManagerTest {
    static RealtyManager realtyManager = new RealtyManager();

    static FlatModel flat1 = new FlatModel(5, 28, 4, true, true, 78, false, false, 7800000, true, true, false);
    static FlatModel flat2 = new FlatModel(9, 10, 3, false, true, 60, true, true, 5300000, true, false, false);

    @Test
    @Order(1)
    void shouldCreate() {
        realtyManager.createFlat(flat1);

        List<FlatModel> expected = new ArrayList<>();
        expected.add(flat1);

        List<FlatModel> actual = realtyManager.grabAll();

        assertEquals(expected, actual);
    }

    @Test
    @Order(2)
    void shouldGetFlatByIdIfExists() {
        FlatModel expected = flat1;
        FlatModel actual = realtyManager.grabById(1);

        assertEquals(expected, actual);
    }

    @Test
    @Order(3)
    void shouldGetFlatByIdIfNotExists() {
        assertThrows(FlatNotFoundException.class, () -> realtyManager.grabById(456));
    }

    @Test
    @Order(4)
    void shouldUpdateFlatIfNotExists() {
        assertThrows(FlatNotFoundException.class, () -> realtyManager.updateFlat(flat2));
    }

    @Test
    @Order(5)
    void shouldUpdateFlatIfExists() {
        flat1.setFlatCostValue(8900000);
        FlatModel expected = flat1;
        FlatModel actual = realtyManager.updateFlat(flat1);

        assertEquals(expected, actual);
    }

    @Test
    @Order(6)
    void shouldGetFlatsSize() {
        realtyManager.createFlat(flat2);

        int expected = 2;
        int actual = realtyManager.countFlats();

        assertEquals(expected, actual);
    }

    @Test
    @Order(7)
    void shouldRemoveFlatByIdIfExists() {

        realtyManager.removeFlatById(1);
        List<FlatModel> expected = new ArrayList<>();
        expected.add(flat2);
        List<FlatModel> actual = realtyManager.grabAll();

        assertEquals(expected, actual);
    }

    @Test
    @Order(8)
    void shouldRemoveFlatByIdIfNotExists() {
        assertFalse(realtyManager.removeFlatById(335));
    }

    @Test
    @Order(9)
    void shouldSearchNotMatchRequest() {
        RequestModel request = new RequestModel(2, 14, 7, 9, true, false, 2, 4, false, false, 45, 98, false, false, 2300000, 6500000, true, true);

        List<FlatModel> expected = new ArrayList<>();
        List<FlatModel> actual = realtyManager.searching(request);

        assertEquals(expected, actual);
    }

    @Test
    @Order(10)
    void shouldSearchNotFreePlannedRequest() {
        RequestModel request = new RequestModel(2, 14, 10, 45, true, false, 2, 4, false, false, 45, 98, false, true, 2300000, 6500000, true, true);

        List<FlatModel> expected = new ArrayList<>();
        List<FlatModel> actual = realtyManager.searching(request);

        assertEquals(expected, actual);
    }

    @Test
    @Order(11)
    void shouldSearchHasNoBalconyRequest() {
        RequestModel request = new RequestModel(2, 14, 4, 14, true, true, 2, 4, false, true, 45, 98, false, true, 2300000, 6500000, true, false);

        List<FlatModel> expected = new ArrayList<>();

        List<FlatModel> actual = realtyManager.searching(request);

        assertEquals(expected, actual);
    }

    @Test
    @Order(12)
    void shouldSearchHasNoLoggiaRequest() {
        RequestModel request = new RequestModel(2, 14, 4, 14, true, true, 2, 4, false, true, 45, 98, true, false, 2300000, 6500000, true, false);

        List<FlatModel> expected = new ArrayList<>();
        List<FlatModel> actual = realtyManager.searching(request);

        assertEquals(expected, actual);

    }

    @Test
    @Order(13)
    void shouldSearchIfNoMoreInfo() {

        RequestModel request = new RequestModel(2, 14, 4, 14, true, true, 2, 4, false, true, 45, 98, true, true, 2300000, 6500000, false, true);

        List<FlatModel> expected = new ArrayList<>();
        List<FlatModel> actual = realtyManager.searching(request);

        assertEquals(expected, actual);
    }

    @Test
    @Order(14)
    void shouldSearchIfNoLocation() {

        RequestModel request = new RequestModel(2, 14, 4, 14, true, true, 2, 4, false, true, 45, 98, true, true, 2300000, 6500000, true, true);

        List<FlatModel> expected = new ArrayList<>();
        List<FlatModel> actual = realtyManager.searching(request);

        assertEquals(expected, actual);
    }

    @Test
    @Order(15)
    void shouldSearchNotStudioRequest() {
        RequestModel request = new RequestModel(2, 14, 10, 45, true, false, 2, 4, true, false, 45, 98, false, false, 2300000, 6500000, true, true);

        List<FlatModel> expected = new ArrayList<>();
        List<FlatModel> actual = realtyManager.searching(request);

        assertEquals(expected, actual);
    }

    @Test
    @Order(16)
    void shouldSearchLessThanMinSquareRequest() {
        RequestModel request = new RequestModel(2, 14, 9, 30, true, true, 3, 4, false, true, 81, 82, true, true, 2300000, 10000000, true, true);

        List<FlatModel> expected = new ArrayList<>();
        List<FlatModel> actual = realtyManager.searching(request);

        assertEquals(expected, actual);
    }

    @Test
    @Order(17)
    void shouldSearchMoreThanMaxSquareRequest() {
        RequestModel request = new RequestModel(2, 14, 9, 30, true, true, 3, 4, false, true, 20, 21, true, true, 2300000, 10000000, true, true);

        List<FlatModel> expected = new ArrayList<>();
        List<FlatModel> actual = realtyManager.searching(request);

        assertEquals(expected, actual);
    }

    @Test
    @Order(18)
    void shouldSearchLessThanMinFloorValueRequest() {
        RequestModel request = new RequestModel(1, 2, 9, 30, true, true, 3, 4, false, true, 20, 21, true, true, 2300000, 10000000, true, true);

        List<FlatModel> expected = new ArrayList<>();
        List<FlatModel> actual = realtyManager.searching(request);

        assertEquals(expected, actual);
    }

    @Test
    @Order(19)
    void shouldSearchMoreThanMaxFloorValueRequest() {
        RequestModel request = new RequestModel(10, 11, 9, 30, true, true, 3, 4, false, true, 20, 21, true, true, 2300000, 10000000, true, true);

        List<FlatModel> expected = new ArrayList<>();
        List<FlatModel> actual = realtyManager.searching(request);

        assertEquals(expected, actual);
    }

    @Test
    @Order(20)
    void shouldSearchLessThanMinFloorInHouseValueRequest() {
        RequestModel request = new RequestModel(2, 14, 39, 40, true, true, 3, 4, false, true, 20, 21, true, true, 2300000, 10000000, true, true);

        List<FlatModel> expected = new ArrayList<>();
        List<FlatModel> actual = realtyManager.searching(request);

        assertEquals(expected, actual);
    }

    @Test
    @Order(21)
    void shouldSearchMoreThanMaxFloorInHouseValueRequest() {
        RequestModel request = new RequestModel(6, 7, 39, 40, true, true, 3, 4, false, true, 20, 21, true, true, 2300000, 10000000, true, true);

        List<FlatModel> expected = new ArrayList<>();
        List<FlatModel> actual = realtyManager.searching(request);

        assertEquals(expected, actual);
    }


    @Test
    @Order(22)
    void shouldSearchFirstFloorRequest() {

        flat2.setFlatFloorValue(1);
        realtyManager.updateFlat(flat2);
        System.out.println("Flat 2 Object" + " " + flat2);

        RequestModel request = new RequestModel(1, 11, 12, 12, false, true, 2, 4, false, true, 40, 80, true, true, 2300000, 10000000, true, false);

        List<FlatModel> expected = new ArrayList<>();
        List<FlatModel> actual = realtyManager.searching(request);

        assertEquals(expected, actual);

    }


    @Test
    @Order(23)
    void shouldSearchNotFirstFloorRequest() {

        flat2.setFlatFloorValue(1);
        realtyManager.updateFlat(flat2);
        System.out.println("Flat 2 Object" + " " + flat2);

        RequestModel request = new RequestModel(1, 11, 5, 12, true, true, 2, 4, false, true, 40, 80, true, true, 2300000, 10000000, true, false);

        List<FlatModel> expected = new ArrayList<>();
        List<FlatModel> actual = realtyManager.searching(request);

        assertEquals(expected, actual);

    }



    @Test
    @Order(24)
    void shouldSearchNotLastFloorRequest() {

        flat2.setFlatFloorValue(10);
        realtyManager.updateFlat(flat2);

        RequestModel request = new RequestModel(2, 10, 5, 15, true, true, 3, 4, false, true, 20, 80, true, true, 2300000, 10000000, true, false);

        List<FlatModel> expected = new ArrayList<>();
        List<FlatModel> actual = realtyManager.searching(request);

        assertEquals(expected, actual);

        flat1.setFlatFloorValue(6);
        realtyManager.updateFlat(flat2);
    }

    @Test
    @Order(25)
    void shouldSearchLessThanMinFlatCostRequest() {

        RequestModel request = new RequestModel(9, 12, 8, 13, true, false, 2, 4, false, true, 20, 80, true, true, 5500000, 5500000, true, false);

        List<FlatModel> expected = new ArrayList<>();

        List<FlatModel> actual = realtyManager.searching(request);

        assertEquals(expected, actual);

    }

    @Test
    @Order(26)
    void shouldSearchIfMoreThanMaxFlatCostRequest() {
        RequestModel request = new RequestModel(9, 12, 8, 13, true, false, 2, 4, false, true, 20, 80, true, true, 1000000, 1900000, true, false);

        List<FlatModel> expected = new ArrayList<>();
        List<FlatModel> actual = realtyManager.searching(request);

        assertEquals(expected, actual);
    }

    @Test
    @Order(27)
    void shouldSearchMoreAndEqualsMaxRoomsRequest() {

        RequestModel request = new RequestModel(9, 12, 8, 13, true, false, 1, 101, false, true, 20, 80, true, true, 1000000, 1900000, true, false);
        realtyManager.searching(request);

        int expected = 99;
        int actual = request.getRoomsMaxValue();

        assertEquals(expected, actual);
    }

    @Test
    @Order(28)
    void shouldSearchLessThanMinRoomsRequest() {
        RequestModel request = new RequestModel(9, 12, 8, 13, true, false, 5, 6, false, true, 20, 80, true, true, 1000000, 1900000, true, false);
        realtyManager.searching(request);

        int expected = 6;
        int actual = request.getRoomsMaxValue();

        assertEquals(expected, actual);
    }

    @Test
    @Order(29)
    void shouldSearchMoreThanMaxRoomsRequest() {

        RequestModel request = new RequestModel(9, 12, 8, 13, true, false, 1, 1, false, true, 20, 80, true, true, 1000000, 1900000, true, false);
        realtyManager.searching(request);

        int expected = 1;
        int actual = request.getRoomsMaxValue();

        assertEquals(expected, actual);
    }

    @Test
    @Order(30)
    void shouldSearchFlatsOverLimitRequest() {
        FlatModel flat4 = new FlatModel(4, 9, 1, false, false, 30, true, false, 2300000, true, true, false);
        FlatModel flat6 = new FlatModel(7, 25, 3, false, false, 73, true, false, 9880000, true, true, false);
        FlatModel flat7 = new FlatModel(9, 26, 2, false, false, 40, true, false, 4300000, true, true, false);
        FlatModel flat8 = new FlatModel(11, 28, 3, false, false, 69, true, false, 7800000, true, true, false);
        FlatModel flat9 = new FlatModel(11, 28, 3, false, false, 69, true, false, 7800000, true, true, false);
        FlatModel flat10 = new FlatModel(11, 28, 3, false, false, 69, true, false, 7800000, true, true, false);
        FlatModel flat11 = new FlatModel(11, 28, 3, false, false, 69, true, false, 7800000, true, true, false);

        RequestModel request2 = new RequestModel(1, 22, 5, 35, true, true, 1, 10, false, false, 20, 203, true, false, 100, 10000000, true, true);

        realtyManager.createFlat(flat4);
        realtyManager.createFlat(flat11);
        realtyManager.createFlat(flat6);
        realtyManager.createFlat(flat7);
        realtyManager.createFlat(flat8);
        realtyManager.createFlat(flat9);
        realtyManager.createFlat(flat10);

        List<FlatModel> expected = new ArrayList<>();
        expected.add(flat4);
        expected.add(flat11);
        expected.add(flat6);
        expected.add(flat7);
        expected.add(flat8);
        expected.add(flat9);
        expected.add(flat10);

        List<FlatModel> actual = realtyManager.searching(request2);

        assertEquals(expected, actual);
    }

}
