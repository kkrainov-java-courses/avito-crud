package org.example.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class RequestModel {

    public static final int MAX_ROOMS_NUMBER_SEARCH = 7;

    private int flatFloorMinValue;
    private int flatFloorMaxValue;

    private int floorsInHouseMinValue;
    private int floorsInHouseMaxValue;

    private boolean notFirstFloorValue;
    private boolean notLastFloorValue;

    private int roomsMinValue;
    private int roomsMaxValue;

    private boolean studioValue;
    private boolean freePlannedValue;

    private int wholeSquareMinValue;
    private int wholeSquareMaxValue;

    private boolean balconyValue;
    private boolean loggiaValue;

    private int minCostValue;
    private int maxCostValue;

    private boolean hasMoreInfoValue;
    private boolean hasLocationValue;

}



